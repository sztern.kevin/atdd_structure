# Compiler flags, implicitly used by the %.o: %.c rule
CFLAGS = -Wall -Wextra -Werror -pedantic -std=c99

# Preprocessor flags, implicitly used by the %.o: %.c rule
CPPFLAGS = -D_POSIX_C_SOURCE=200809L -Isrc/

# We will build two binaries:
# the final binary that can be used by our client, and
# the developer-exclusive testsuite that runs our unit tests.
# Two different linking phases, a pair of linker vars for each.

# Linker flags when we link the real binary
LDFLAGS =
# Libs to use during the linking phase of the real binary
LDLIBS =

# Linker flags when we link our unit testsuite
TEST_LDFLAGS =
# Libs to use during the linking phase of the unit testsuite
TEST_LDLIBS = -lcriterion

# All objs except main.o, to be sure we don't include it in our unit tests
OBJS = $(addprefix src/, toupper.o)

TEST_OBJS = $(addprefix tests/unit/, tests_toupper.o)

all: toto

toto: $(OBJS) src/main.o
	$(CC) -o $@ $^

check: unit_check func_check

unit_check: unit_testsuite
	./unit_testsuite --verbose

unit_testsuite: $(OBJS) $(TEST_OBJS)
	$(CC) -o $@ $^ $(TEST_LDFLAGS) $(TEST_LDLIBS)


func_check: toto
	pharaoh tests/func

clean:
	rm -f $(OBJS) src/main.o $(TEST_OBJS) unit_testsuite toto

.PHONY: all check unit_check func_check clean
