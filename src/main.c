#include <stdio.h>
#include <stdlib.h>

#include "toupper.h"

/**
** The main.c is the border between the outside world and
** the core logic of our code.
**
** We should aim to make it as thin as possible, since it will not be included
** in the unit tests. Do the outside world related paperwork, read your lines,
** then send them off to your happy core code.
**
** The core code doesn't print anything, that is infra stuff. So we take care
** of printing the result ourselves.
*/


int main(void)
{
  char *line = NULL;
  size_t len = 0;
  ssize_t nread;

  while ((nread = getline(&line, &len, stdin)) != -1)
  {
    line[nread - 1] = '\0';
    toupper_line(line);
    puts(line);
  }
  free(line);
  return 0;
}
