#include <criterion/criterion.h>

#include <stdlib.h>

#include "toupper.h"

Test(toupper, onechar)
{
  // GIVEN
  char s[] = "u";
  // WHEN
  toupper_line(s);
  // THEN
  cr_assert_str_eq(s, "U");
}
